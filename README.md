# verdantcurve

This is the verdantcurve Agent-Based Model (ABM) for financial markets.
It implements a batch auction, orderbooks, some simple agents, and a market 
framework to hold them all together.

### Installation
```bash
pip install git+https://gitlab.com/daviddewhurst/verdantcurve.git
```
If you want more control over where the files land or Git behavior:
```bash
git clone https://gitlab.com/daviddewhurst/verdantcurve.git <directory>
cd <directory>
pip install .
```

Without further ado, you can run your own simulation:
```
simulation -h
usage: simulation [-h] [-d DAYS] [-s STEPS_PER_DAY] [-zi N_ZI] [-zip N_ZIP]
                  [-mo N_MO] [-mr N_MR] [-mm N_MM] [-fv N_FV] [-nn N_NN]
                  [-v]

Simulates an isolated stock market for a single stock that operates using the
Frequent Batch Auction mechanism.

optional arguments:
  -h, --help            show this help message and exit
  -d DAYS, --days DAYS  Determines the number of trading days simulated.
                        (default: 252)
  -s STEPS_PER_DAY, --steps_per_day STEPS_PER_DAY
                        Determines the number of simulation steps in a single
                        trading day. (default: 390)
  -zi N_ZI, --n_zi N_ZI
                        Determines the number of zero intelligence agents in
                        the initial population. (default: 60)
  -zip N_ZIP, --n_zip N_ZIP
                        Determines the number of zero intelligence agents
                        (that utilize market orders) in the initial
                        population. (default: 0)
  -mo N_MO, --n_mo N_MO
                        Determines the number of momentum agents in the
                        initial population. (default: 60)
  -mr N_MR, --n_mr N_MR
                        Determines the number of mean reversion agents in the
                        initial population. (default: 60)
  -mm N_MM, --n_mm N_MM
                        Determines the number of market maker agents in the
                        initial population. (default: 60)
  -fv N_FV, --n_fv N_FV
                        Determines the number of fundamental value agents in
                        the initial population. (default: 60)
  -nn N_NN, --n_nn N_NN
                        Determines the number of neural network agents in the
                        initial population. (default: 60)
  -v, --verbose         Determines the level of terminal output. (default: 0)
```

If you write your own code and return orderbooks for further analysis, there is some code to make plotting and statistical analysis a little easier:

```
analyze_orderbook -h
usage: analyze_orderbook [-h] -f FILES [FILES ...] -s SAVEPATHS
                         [SAVEPATHS ...] [-v] [-a]
                         [-p [PRICEPATHS [PRICEPATHS ...]]]

Visualize and build orderbooks that result from a `markets.Market` simulation.

optional arguments:
  -h, --help            show this help message and exit
  -f FILES [FILES ...], --files FILES [FILES ...]
                        path(s) to orderbook objects (loadable by
                        `joblib.load`) (default: None)
  -s SAVEPATHS [SAVEPATHS ...], --savepaths SAVEPATHS [SAVEPATHS ...]
                        path(s) to which to save output (default: None)
  -v, --visualize       Whether or not to visualize the orderbook (default:
                        False)
  -a, --analyze         Whether or not to analyze the orderbook (default:
                        False)
  -p [PRICEPATHS [PRICEPATHS ...]], --pricepaths [PRICEPATHS [PRICEPATHS ...]]
                        Optional paths to files storing price information. If
                        included must be same length as number of orderbooks.
                        (default: None)
```

## Contributing
In alphabetical order only.
+ [David Rushing Dewhurst](http://davidrushingdewhurst.com)
+ [Colin Van Oort](https://gitlab.com/omega1563)
