import json
import logging
import random
from collections import defaultdict

from sortedcollections import SortedList

NaO = "NaO"
NaP = "NaP"


def read_json(f):
    """
    Converts a json object into an Order.
    """
    obj = json.load(f)
    return Order(
        str(obj["uid"]),
        str(obj["oid"]),
        str(obj["side"]),
        int(obj["shares"]),
        float(obj["price"]),
        str(obj["o_type"]),
        int(obj["time_in_force"]),
    )


class Order:
    """
    Stores data associated with an order in a financial market. Orders fall on the
    buy side (bid) or the sell side (ask) and include information about the desired
    price and quantity, as well as an identifier associated with the participant
    who created the order.
    """

    def __init__(self, uid, oid, side, shares, price, o_type="limit", time_in_force=5):
        self.uid = uid  # Keep track of who submitted the order
        self.oid = oid
        self.side = side
        self.price = price
        self.o_type = o_type
        self.shares = shares
        self.random = random.random()

        self.submit_time = -1
        self.time_in_force = time_in_force

    def __repr__(self):
        return (
            f"Order({self.uid}, {self.oid}, {self.side}, {self.shares}, "
            f"{self.price}, {self.o_type}, {self.time_in_force})"
        )

    def __str__(self):
        return (
            f"{self.o_type} {self.side} order {self.oid} for {self.shares} shares @ ${self.price}"
            f" from agent {self.uid}"
        )

    def to_json(self, path=None):
        """
        Args:
            path: pathlib.Path or None, Allows export of the JSON object to a file.

        Returns: dict
        """
        obj = {
            "uid": self.uid,
            "oid": self.oid,
            "side": self.side,
            "shares": self.shares,
            "price": self.price,
            "o_type": self.o_type,
            "time_in_force": self.time_in_force,
        }
        if path is not None:
            path.parent.mkdir(parents=True, exist_ok=True)
            with open(path, "w") as f:
                json.dump(obj, f)
        return obj


class OrderBook:
    """
    Implements a limit order book for use in continuous and batched double auctions.
    """

    def __init__(self):
        self.bids = SortedList(key=lambda x: (-x.price, x.submit_time, x.random))
        self.asks = SortedList(key=lambda x: (x.price, x.submit_time, x.random))

    def get_consolidated_book(self):
        """
        Provides one dictionary for each side of the market denoting the current
        quantity resting at each price point.

        Returns: Tuple[defaultdict[float, int], defaultdict[float, int]]
            A consolidated view of both sides of the book.
        """
        bids, asks = defaultdict(int), defaultdict(int)
        for bid in self.bids:
            bids[bid.price] += bid.shares

        for ask in self.asks:
            asks[ask.price] += ask.shares

        return bids, asks

    def add_order(self, order):
        """
        Add a single order to the book.

        Args:
            order: Order
        """
        if order.side == "bid":
            self.bids.add(order)
        elif order.side == "ask":
            self.asks.add(order)
        else:
            raise ValueError(f'Expected "bid" or "ask" order, received {order.side}.')
        logging.info(self.format_add(order))

    def add_orders(self, orders):
        """
        Adds all orders in a list to the book.

        Args:
            orders: List[Order]
        """
        for order in orders:
            self.add_order(order)

    def remove_order(self, order, time, mod=False, raise_=False):
        """
        Removes a single order from the book.

        Args:
            order: Order
            time: numeric
            mod: bool
            raise_: bool
        """
        if order.side == "bid":
            if raise_:
                self.bids.remove(order)
            else:
                try:
                    self.bids.remove(order)
                except ValueError as e:
                    print(f"Error when removing bid order: {e}")
        elif order.side == "ask":
            if raise_:
                self.asks.remove(order)
            else:
                try:
                    self.asks.remove(order)
                except ValueError as e:
                    print(f"Error when removing ask order: {e}")
        else:
            raise ValueError(f'Expected "bid" or "ask" order, received {order.side}.')

        if mod:
            logging.info(self.format_modify(order, 0, time))

    def remove_orders(self, orders, time, mod=False, raise_=False):
        """
        Removes all orders in a list from the book.

        Args:
            orders: List[Order]
            time: numeric, Current simulation time
            mod: bool, Toggles the issuance of modify messages for each removal.
            raise_: bool, Allows failed removal errors to propagate upwards.
        """
        for order in orders:
            self.remove_order(order, time, mod=mod, raise_=raise_)

    def remove_stale(self, time):
        """
        Removes all stale orders from the book, where staleness is defined using
        an arbitrary threshold over the lifetime of orders.

        Args:
            time: numeric
        """
        stale_orders = [
            o
            for o in (self.bids + self.asks)
            if time >= o.submit_time + o.time_in_force
        ]
        logging.info(f"Removing {len(stale_orders)} stale orders.")
        self.remove_orders(stale_orders, time, mod=True)

    def clear_book(self, time):
        """
        Places a message in the log and clears the book.
        Args:
            time: numeric, Current time.
        """
        logging.info(f"Clearing book: {time}")
        self.__init__()

    def remove_agent(self, uid, time):
        """
        Removes all orders associated with a particular agent from the book.

        Args:
            uid: hashable, Identifier associated with the agent to be removed.
            time: numeric, Current simulation time.
        """
        agent_orders = [o for o in (self.bids + self.asks) if o.uid == uid]
        self.remove_orders(agent_orders, time, mod=True)

    def crossed_market(self):
        """
        Indicates whether the market is currently crossed, e.g. the best bid has
        a higher price than the best ask.

        Returns: Boolean
        """
        try:
            return self.bids[0].price > self.asks[0].price
        except IndexError:
            return False

    def locked_market(self):
        """
        Indicates whether the market is currently locked, e.g. the best bid and
        best ask have the same price.

        Returns: Boolean
        """
        try:
            return self.bids[0].price == self.asks[0].price
        except IndexError:
            return False

    def cleared_market(self):
        """
        Indicates whether the market is currently cleared, e.g. there are no
        crossing orders.

        Returns: Boolean
        """
        try:
            return self.bids[0].price < self.asks[0].price
        except IndexError:
            return False

    @staticmethod
    def format_add(order):
        """
        Creates a log message, formatted as a JSON dictionary, for an order added to the book.
        Args:
            order: Order, An order that has just been added to the book.

        Returns: str
        """
        return (
            f"{{time: {order.submit_time}, mtype: add, shares: {order.shares}, "
            f"price: {order.price}, oid: {order.oid}, side: {order.side}}}"
        )

    @staticmethod
    def format_modify(order, shares, time):
        """
        Creates a log message, formatted as a JSON dictionary, for a resting order that has been modified.
        Args:
            order: Order, A resting order that has just been modified.
            shares: int, Remaining shares after modification.
            time: int, Current simulation time.

        Returns: str
        """
        assert (
            shares <= order.shares
        ), f"shares = {shares} but order shares = {order.shares}"
        return (
            f"{{time: {time}, mtype: modify, shares: {shares}, "
            f"price: {order.price}, oid: {order.oid}, side: {order.side}}}"
        )

    def __contains__(self, item):
        return (item in self.bids) or (item in self.asks)
