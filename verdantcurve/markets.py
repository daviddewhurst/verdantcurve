import logging
import random

from . import engine, orders


class Market:
    """
    Provides an interface that combines and controls a limit order book,
    a matching engine, and a pool of active agents.
    """

    def __init__(
        self, agents=None, book=None, m_engine=None,
    ):
        self.registered_agents = agents or []
        self.book = book or orders.OrderBook()
        self.engine = m_engine or engine.BatchEngine()
        self._add_engine_to_registered_agents()

        self.current_time = 0
        self.uid2visible_result_attrs = dict()

    def _add_engine_to_registered_agents(self):
        for agent in self.registered_agents:
            agent.add_engine(self.engine)

    def _set_agents_visible_attributes(self, agents):
        for agent in agents:
            self.uid2visible_result_attrs[agent.uid] = agent.visible

    def register_agents(self, agents):
        """
        Extends the list of active agents with one or more additional agents.

        Args:
            agents: List[Agent]
        """
        logging.info("{} new agents entering market".format(len(agents)))
        self.registered_agents.extend(agents)
        self._add_engine_to_registered_agents()
        self._set_agents_visible_attributes(agents)

    def request_agent_attributes(self, uid):
        ...

    def delete_agents(self, agents, time):
        """
        Removes one or more agents from the list of active agents. Ensures that
        all orders associated with removed agents are removed from the limit
        order book.

        Args:
            agents: List[Agent]
            time: numeric, Current time.
        """
        for agent in agents:
            self.book.remove_agent(agent.uid, time)

            try:
                self.registered_agents.remove(agent)
                logging.info("agent {} left the market".format(agent.uid))
            except ValueError:
                logging.info(
                    "agent {} did not reach the market before removal".format(agent.uid)
                )

    def step(
        self, t, dynamic_book=False, side_effects=None, recorders=None,
    ):
        """
        Executes one discrete time unit of a market simulation. Optionally returns a view of the 
        order book after matching is complete. Optionally applies callables that alter market 
        state at each iteration after matching is complete. 
        Optionally applies callables that record statistics of market at each iteration after
        matching is complete.
        Callables must take a Market as a single argument.

        Args:
            t: numeric, Current time.
            dynamic_book: bool, Returns a consolidated view of the book if True.
            side_effects: list of callables, Applies callables to Market if not empty.
            recorders: list of callables, Applies callables to Market if not empty.

        Returns: None or Tuple[Dict[float, int]]
        """
        side_effects = side_effects or []
        recorders = recorders or []
        result_holder = []
        self.current_time = t

        for agent in random.sample(self.registered_agents, len(self.registered_agents)):
            self.engine.submit(
                agent.trade(self.engine.eid), self.book, t,
            )

        pmb = self.book.get_consolidated_book()

        self.engine.match(self.book, t)
        self.book.remove_stale(t)
        self.engine.update_luld_bands()

        for agent in self.registered_agents:
            if agent.uid in self.engine.agent_updates.keys():
                agent.update(self.engine.eid)

        order_count = len(self.book.bids) + len(self.book.asks)
        logging.info(
            f"{order_count} orders, "
            f"{100 * len(self.book.bids) / (order_count + 1e-5):0.2f}% bids, "
            f"{100 * len(self.book.asks) / (order_count + 1e-5):0.2f}% asks"
        )

        # apply side effects to market
        for function in side_effects:
            function(self)

        # apply recorders and store statistics
        if recorders:
            for function in recorders:
                results = function(self)
                result_holder.append(results)

        if dynamic_book:
            if recorders:
                bids, asks = self.book.get_consolidated_book()
                return bids, asks, result_holder, pmb
            else:
                bids, asks = self.book.get_consolidated_book()
                return bids, asks, pmb
        if recorders:
            return result_holder, pmb
        return pmb

    def run(
        self,
        days,
        steps_per_day,
        dynamic_book=False,
        side_effects=None,
        recorders=None,
        pre_match_book=False,
    ):
        """
        Executes several time steps of a discrete time market simulation.

        Args:
            days: int, Number of trading days to simulate.
            steps_per_day: int, Number of batches executed per trading day.
            dynamic_book: bool, Collects and returns a consolidated view of the book over time if True.
            side_effects: List[Callable], Side effects to be applied to the Market.
            recorders: List[Callable], Recorders to be applied to the Market.
            pre_match_book: bool, returns consolidated pre-match book if True

        Returns: None or Tuple[List[Dict[float, int]], List[Dict[float, int]]]
        """
        if not self.registered_agents:
            raise ValueError(
                "Must register agents with market before running simulation"
            )

        side_effects = side_effects or []
        recorders = recorders or []
        pre_match_books = []
        result_holder = []
        t = 0

        if not dynamic_book:
            if recorders:

                for day in range(days):
                    for s in range(steps_per_day):
                        t = day * steps_per_day + s
                        results, pre_match_book = self.step(
                            t, side_effects=side_effects, recorders=recorders,
                        )
                        pre_match_books.append(pre_match_book)
                        result_holder.append(results)
                    self.book.clear_book(t)
                return (
                    result_holder
                    if not pre_match_book
                    else (result_holder, pre_match_books)
                )
            else:
                for day in range(days):
                    for s in range(steps_per_day):
                        t = day * steps_per_day + s
                        pre_match_book = self.step(t, side_effects=side_effects)
                        pre_match_books.append(pre_match_book)
                    self.book.clear_book(t)
                return pre_match_books if pre_match_book else None
        else:
            bbids = []
            aasks = []
            if recorders:
                result_holder = []
                for day in range(days):
                    for s in range(steps_per_day):
                        t = day * steps_per_day + s
                        bids, asks, results, pre_match_book = self.step(
                            t,
                            dynamic_book=True,
                            side_effects=side_effects,
                            recorders=recorders,
                        )
                        pre_match_books.append(pre_match_book)
                        result_holder.append(results)
                        bbids.append(bids)
                        aasks.append(asks)
                    self.book.clear_book(t)
                return (
                    (bbids, aasks, result_holder)
                    if not pre_match_book
                    else (bbids, aasks, result_holder, pre_match_books)
                )
            else:
                for day in range(days):
                    for s in range(steps_per_day):
                        t = day * steps_per_day + s
                        bids, asks, pre_match_book = self.step(
                            t, dynamic_book=True, side_effects=side_effects,
                        )
                        pre_match_books.append(pre_match_book)
                        bbids.append(bids)
                        aasks.append(asks)
                    self.book.clear_book(t)
                return (
                    (bbids, aasks)
                    if not pre_match_book
                    else (bbids, aasks, pre_match_books)
                )
