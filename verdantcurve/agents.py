import numpy as np
from scipy import stats

from . import agent_architectures as aa
from . import orders


class Agent:
    """
    Implements an abstract base class for a hierarchy of agent_updates that interact
    with Markets and Matching Engines in order to trade goods.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"a{cls.count}"

    def __init__(self, uid=None, visible=tuple()):
        """
        Args:
            uid: hashable, A unique identifier to be associated with the agent.
                Default behavior is to assign a non-negative integer that is
                managed through a class variable.
            visible: tuple, attributes that will be visible in `market.engine.agent_updates` to other agents
        """
        self.uid = uid or type(self).next_id()
        self.visible = visible
        self.engines = {}

        # accounting
        self.cash = 0
        self.counter_parties = []
        self.order_number = 0
        self.shares_held = 0
        self.profit = 0

        self.evolvable_parameters = []
        self.param_bounds = []

    def __repr__(self):
        return "Agent"

    def add_engine(self, engine):
        """
        Args:
            engine: engine.MatchingEngine
        """
        self.engines[engine.eid] = engine

    def trade(self, eid):
        """
        Args:
            eid: hashable, Unique identifier of the target exchange.
        """
        pass

    def update(self, eid):
        """
        Updates agent holdings based on result messages from a given exchange.

        Args:
            eid: hashable, Unique identifier of the target exchange.
        """
        reports = self.engines[eid].agent_updates[self.uid]
        for report in reports:
            if report.side == "bid":
                self.shares_held += report.shares
                self.cash -= report.shares * report.price
            else:
                self.shares_held -= report.shares
                self.cash += report.shares * report.price
            self.counter_parties.append(report.poid.split("-")[0])
        self.profit += self.cash + self.engines[eid].eq * self.shares_held


class FFNNAgent(Agent):
    """
    Implements a simple FFNN agent.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"fna{cls.count}"

    def __init__(self, uid=None, n_hidden=20, train_status=False, **kwargs):
        super().__init__(uid)
        self.n_hidden = n_hidden

        self.ndims = 6
        self.output_activation = aa.db_trade_activation
        if "model" in kwargs:
            self.model = kwargs.get("model")
            self.loaded = True
        else:
            self.model = aa.FFNN(
                layer_dims=(self.ndims, n_hidden, int(n_hidden / 2), 3),
                output_activation=self.output_activation,
            )
            self.loaded = False
        self.evolvable_parameters = ["model.layers", "model.biases"]
        self.param_bounds = [aa.NoBound(), aa.NoBound()]

        # initialize things to ghost values
        self.last = float(np.random.randint(80, 120 + 1))
        self.last_order = orders.NaO
        self.last_shares = 100
        self.last_cash = 0
        self.last_shares_held = 0
        self.last_bid_volume = 0
        self.last_ask_volume = 0
        self.last_profit = 0

        # accounting for how nn was trained
        self.train_status = train_status

    def save(self, path):
        self.model.save(path)

    def __repr__(self):
        return f"FFNN"

    def set_train(self, train):
        self.train_status = train

    def _make_nn_input(self, eid, p):
        if self.last_order == orders.NaO:
            self.last_shares = 100
        else:
            self.last_shares = self.last_order.shares

        # get the orderbook
        try:
            bids, asks = self.engines[eid].book.get_consolidated_book()
            bid_volume = np.sum(list(bids.values()))
            ask_volume = np.sum(list(asks.values()))
        except AttributeError:
            # the book hasn't yet beeen initialized
            bid_volume = 0.0
            ask_volume = 0.0

        # these are actually percent change
        d_bid_volume = bid_volume - self.last_bid_volume
        d_ask_volume = ask_volume - self.last_ask_volume
        dp = p - self.last
        d_cash = self.cash - self.last_cash
        d_shares_held = self.shares_held - self.last_shares_held
        d_profit = self.profit - self.last_profit

        arr = np.array(
            [d_bid_volume, d_ask_volume, dp, d_cash, d_shares_held, d_profit]
        )

        self.last_bid_volume = bid_volume
        self.last_ask_volume = ask_volume

        if self.train_status:
            rv = 0.25 * arr * np.random.randn(arr.shape[0])
            arr = arr + rv
        return arr

    def trade(self, eid, train=False):
        """
        Makes trading decisions using output of the FFNN.

        Args:
            eid: hashable, Unique identifier of the target exchange.
            train: bool

        Returns: Order
        """
        self.set_train(train)

        p = self.engines[eid].eq
        if (p == orders.NaP) or (self.last == orders.NaP):
            return []

        # inputs: last dp, last nshares
        inputs = self._make_nn_input(eid, p)
        outputs = self.model.forward(inputs)

        # clean up the outputs for actual trading
        # outputs: buy / sell, +/- shares, +/- price
        side = "bid" if outputs[0] > 0 else "ask"
        shares = max(int(self.last_shares + outputs[1]), 1)
        price = max(np.round(self.last + outputs[-1], 2), 0.01)

        order = orders.Order(
            self.uid,
            f"{self.uid}-{self.order_number}",
            side,
            shares,
            max(np.round(price, 2), 0.01),
            time_in_force=0,
        )

        self.last = p
        self.last_order = order
        self.order_number += 1
        self.last_cash = self.cash
        self.last_shares_held = self.shares_held
        self.last_profit = self.profit

        return [order]


class FFNNPAgent(Agent):
    """
    Implements a simple priceless (market order) FFNN agent.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"fnp{cls.count}"

    def __init__(self, uid=None, n_hidden=20, train_status=False, shares=100):
        super().__init__(uid)
        self.shares = shares
        self.n_hidden = n_hidden

        self.ndims = 6
        self.output_activation = np.tanh
        self.model = aa.FFNN(
            layer_dims=(self.ndims, n_hidden, int(n_hidden / 2), 3),
            output_activation=self.output_activation,
        )
        self.evolvable_parameters = ["model.layers", "model.biases"]
        self.param_bounds = [aa.NoBound(), aa.NoBound()]

        # initialize things to ghost values
        self.last = float(np.random.randint(80, 120 + 1))
        self.last_order = orders.NaO
        self.last_shares = 100
        self.last_cash = 0
        self.last_shares_held = 0
        self.last_bid_volume = 0
        self.last_ask_volume = 0
        self.last_profit = 0

        self.train_status = train_status

    def __repr__(self):
        return f"FFNNP"

    def set_train(self, train):
        self.train_status = train

    def _make_nn_input(self, eid, p):
        if self.last_order == orders.NaO:
            self.last_shares = 100
        else:
            self.last_shares = self.last_order.shares

        # get the orderbook
        try:
            bids, asks = self.engines[eid].book.get_consolidated_book()
            bid_volume = np.sum(list(bids.values()))
            ask_volume = np.sum(list(asks.values()))
        except AttributeError:
            # the book hasn't yet beeen initialized
            bid_volume = 0.0
            ask_volume = 0.0

        d_bid_volume = bid_volume - self.last_bid_volume
        d_ask_volume = ask_volume - self.last_ask_volume
        dp = p - self.last
        d_cash = self.cash - self.last_cash
        d_shares_held = self.shares_held - self.last_shares_held
        d_profit = self.profit - self.last_profit

        arr = np.array(
            [d_bid_volume, d_ask_volume, dp, d_cash, d_shares_held, d_profit]
        )

        self.last_bid_volume = bid_volume
        self.last_ask_volume = ask_volume

        if self.train_status:
            rv = 0.25 * arr * np.random.randn(arr.shape[0])
            arr = arr + rv
        return arr

    def trade(self, eid, train=False):
        """
        Makes trading decisions using output of the FFNN.

        Args:
            eid: hashable, Unique identifier of the target exchange.
            train: bool

        Returns: Order
        """
        self.set_train(train)

        p = self.engines[eid].eq
        if (p == orders.NaP) or (self.last == orders.NaP):
            return []

        # inputs: last dp, last nshares
        inputs = self._make_nn_input(eid, p)
        outputs = self.model.forward(inputs)

        # clean up the outputs for actual trading
        # outputs: buy / sell, +/- shares, +/- price
        side = "bid" if outputs[0] > 0 else "ask"

        order = orders.Order(
            self.uid,
            f"{self.uid}-{self.order_number}",
            side,
            self.shares,
            None,
            o_type="market",
            time_in_force=0,
        )

        self.last = p
        self.last_order = order
        self.order_number += 1
        self.last_cash = self.cash
        self.last_shares_held = self.shares_held
        self.last_profit = self.profit

        return [order]


class Copycat(Agent):

    """
    """

    count = 0




class ZIAgent(Agent):
    """
    Implements a zero-intelligence agent.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"rza{cls.count}"

    def __init__(
        self,
        uid=None,
        pr_trade=None,
        shares=100,
        vol_pref=None,
        pr_bid=0.5,
        rand_shares=True,
    ):
        """
        Args:
            uid: the unique user id of the agent
            pr_trade: the probability of the agent submitting an order in a time period
            shares: the mean number of shares submitted by an agent in an order
            vol_pref: the volume preference of the agent. The agent submits orders with a price
                $order_{t+1} = x_t + vol_pref * u_t$, where $u_t$ is a uniform (-1, 1) random variable and
                $x_t$ is the price at the last time period.
            pr_bid: the probability that the agent submits a bid order, given that the agent submits an order.
            rand_shares: if True, submit Poisson number of shares distributed around `shares`;
                if False, submit shares number of `shares`.
        """
        super().__init__(uid)
        self.pr_trade = pr_trade or np.random.random()
        self.shares = shares
        self.vol_pref = vol_pref or np.random.exponential(10)
        self.pr_bid = pr_bid
        self.rand_shares = rand_shares
        self.evolvable_parameters = ["pr_trade", "shares", "vol_pref", "pr_bid"]
        self.param_bounds = [
            aa.IntervalBound(0.0, 1.0),
            aa.IntervalBound(1, np.inf),
            aa.PositiveRealBound(),
            aa.IntervalBound(0.0, 1.0),
        ]

    def __repr__(self):
        return "ZI"

    def trade(self, eid):
        """
        Args:
            eid: hashable, Unique identifier of the target exchange.

        Returns: Order
        """
        if np.random.random() < self.pr_trade:
            # clipped normal around the deterministic bid prob
            pr_bid = np.clip(self.pr_bid + 0.005 * np.random.randn(), a_min=0, a_max=1)
            side = np.random.choice(["bid", "ask"], p=[pr_bid, 1 - pr_bid])

            p = self.engines[eid].eq
            if p == orders.NaP:
                # randomly initialized about 100
                # for all experiments the initial price should not be NaP
                price = np.random.randint(80, 120 + 1)
            else:
                price = p + self.vol_pref * np.random.random() - self.vol_pref / 2

            price = max(np.round(price, 2), 0.01)

            if self.rand_shares:
                shares = stats.poisson.rvs(mu=self.shares)
            else:
                shares = self.shares

            order = orders.Order(
                self.uid, f"{self.uid}-{self.order_number}", side, shares, price,
            )
            self.order_number += 1
            return [order]
        return []


class ZIPAgent(Agent):
    """
    Implements a zero-intelligence agent that submits only market orders.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"zip{cls.count}"

    def __init__(
        self, uid=None, pr_trade=None, shares=100, pr_bid=0.5, rand_shares=True,
    ):
        """
        Args:
            uid: the unique user id of the agent
            pr_trade: the probability of the agent submitting an order in a time period
            shares: the mean number of shares submitted by an agent in an order
            pr_bid: the probability that the agent submits a bid order, given that the agent submits an order.
            rand_shares: if True, submit Poisson number of shares distributed around `shares`;
                if False, submit shares number of `shares`.
        """
        super().__init__(uid)
        self.pr_trade = pr_trade or np.random.random()
        self.shares = shares
        self.pr_bid = pr_bid
        self.rand_shares = rand_shares
        self.evolvable_parameters = ["pr_trade", "shares", "pr_bid"]
        self.param_bounds = [
            aa.IntervalBound(0.0, 1.0),
            aa.IntervalBound(1, np.inf),
            aa.IntervalBound(0.0, 1.0),
        ]

    def __repr__(self):
        return "ZIP"

    def trade(self, eid):
        """
        Args:
            eid: hashable, Unique identifier of the target exchange.

        Returns: Order
        """
        if np.random.random() < self.pr_trade:
            side = "bid" if np.random.random() < self.pr_bid else "ask"

            if self.rand_shares:
                shares = stats.poisson.rvs(mu=self.shares)
            else:
                shares = self.shares

            order = orders.Order(
                self.uid,
                f"{self.uid}-{self.order_number}",
                side,
                shares,
                None,
                o_type="market",
                time_in_force=0,
            )
            self.order_number += 1
            return [order]
        return []


class MomentumAgent(Agent):
    """
    Implements a basic momentum trading agent that assumes momentum (volume-weighted
    price difference) is autocorrelated.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"ma{cls.count}"

    def __init__(self, uid=None, shares=100, dx=0.05, rand_shares=True):
        """
        Args:
            uid: hashable, Unique identifier associated with the agent.
            shares: float, the mean number of shares to submit in an order.
            dx: float, smallest price increment, used in calculations of momentum.
            rand_shares: if True, submit Poisson number of shares distributed around `shares`;
            if False, submit shares number of `shares`.
        """
        super().__init__(uid)
        self.shares = shares
        self.dx = dx
        self.profit_threshold = -1000
        self.rand_shares = rand_shares
        self.evolvable_parameters = ["shares", "dx", "profit_threshold"]
        self.param_bounds = [
            aa.IntervalBound(1, np.inf),
            aa.IntervalBound(0.01, 0.1),
            aa.IntervalBound(-10000, np.inf),
        ]

        self.last = np.random.randint(80, 120 + 1)
        self.last_order = orders.NaO

    def __repr__(self):
        return "Momentum"

    def trade(self, eid, volume=1.0, dt=1.0):
        """
        Makes trading decisions using momentum calculations.

        Args:
            eid: hashable, Unique identifier of the target exchange.
            volume: (optional) if not None, the number of shares traded in the last time period
            dt: float,

        Returns: Order
        """
        p = self.engines[eid].eq
        if (p == orders.NaP) or (self.last == orders.NaP):
            return []

        # Failure condition should be more sophisticated than "don't trade!"
        if self.shares_held * p + self.cash <= self.profit_threshold:
            return []

        dp = self.last - p
        if dp > 0:
            side = "ask"
            price = p + self.dx
        elif dp < 0:
            side = "bid"
            price = p - self.dx
        else:
            side = np.random.choice(["ask", "bid"])
            price = p

        if self.rand_shares:
            shares = stats.poisson.rvs(mu=self.shares)
        else:
            shares = self.shares

        order = orders.Order(
            self.uid,
            f"{self.uid}-{self.order_number}",
            side,
            shares,
            max(np.round(price, 2), 0.01),
            time_in_force=0,
        )

        self.last = p
        self.last_order = order

        self.order_number += 1
        return [order]


class FundamentalValueAgent(Agent):
    """
    Defines a basic mean-reverting agent. This agent has a set mean price
    (think of this as a prior over prices) and submits orders that respect this
    price's relationship to the current market equilibrium price.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"fva{cls.count}"

    def __init__(
        self,
        uid=None,
        shares=100,
        dx=0.01,
        mean_price=None,
        price_tolerance=0,
        vol_pref=1,
        rand_shares=True,
    ):
        super().__init__(uid)
        self.shares = shares
        self.dx = dx
        self.mean_price = mean_price or np.random.randint(80, 120 + 1)
        self.price_tolerance = price_tolerance
        self.vol_pref = vol_pref
        self.rand_shares = rand_shares
        self.evolvable_parameters = [
            "shares",
            "dx",
            "mean_price",
            "price_tolerance",
            "vol_pref",
        ]
        self.param_bounds = [
            aa.IntervalBound(1, np.inf),
            aa.IntervalBound(0.01, 0.1),
            aa.PositiveRealBound(),
            aa.PositiveRealBound(),
            aa.PositiveRealBound(),
        ]

    def __repr__(self):
        return "FundamentalValue"

    def trade(self, eid):
        p = self.engines[eid].eq

        if p == orders.NaP:
            return []
        elif p > self.mean_price + self.price_tolerance:
            side = "ask"
            price = p + self.dx + self.vol_pref * np.random.random() - self.vol_pref / 2
        elif p < self.mean_price - self.price_tolerance:
            side = "bid"
            price = p - self.dx + self.vol_pref * np.random.random() - self.vol_pref / 2
        else:
            return []

        price = max(np.round(price, 2), 0.01)

        if (type(self.shares) is not str) and (self.rand_shares is True):
            shares = stats.poisson.rvs(mu=self.shares)
        elif self.shares == "prop":
            shares = 10 * int(abs(p - self.mean_price))
        else:
            shares = self.shares

        order = orders.Order(
            self.uid,
            f"{self.uid}-{self.order_number}",
            side,
            shares,
            price,
            time_in_force=0,
        )
        self.order_number += 1
        return [order]


class MeanReversionAgent(Agent):
    """
    This agent estimates the current price of an asset and trades against large
    price changes, anticipating a reversion to some mean.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"mra{cls.count}"

    def __init__(
        self,
        uid=None,
        shares=100,
        window=50,
        price_tolerance=0,
        vol_pref=1,
        rand_shares=True,
    ):
        super().__init__(uid)
        self.shares = shares
        self.window = window
        self.prices = np.zeros(self.window)
        self.current_ind = 0
        self.mean_price = 0
        self.price_tolerance = price_tolerance
        self.vol_pref = vol_pref
        self.rand_shares = rand_shares
        self.evolvable_parameters = ["shares", "price_tolerance", "vol_pref"]
        self.param_bounds = [
            aa.IntervalBound(1, np.inf),
            aa.PositiveRealBound(),
            aa.PositiveRealBound(),
        ]

    def __repr__(self):
        return "MeanReversion"

    def trade(self, eid):
        p = self.engines[eid].eq
        self.prices[self.current_ind] = p
        self.current_ind = (self.current_ind + 1) % self.window

        self.mean_price = self.prices[self.prices > 0].mean()
        if p == orders.NaP:
            return []
        elif p > self.mean_price + self.price_tolerance:
            side = "ask"
            price = p - self.vol_pref * np.random.random()
        elif p < self.mean_price - self.price_tolerance:
            side = "bid"
            price = p + self.vol_pref * np.random.random()
        else:
            return []

        price = max(np.round(price, 2), 0.01)

        if (type(self.shares) is not str) and (self.rand_shares is True):
            shares = stats.poisson.rvs(mu=self.shares)
        elif self.shares == "prop":
            shares = 10 * int(abs(p - self.mean_price))
        else:
            shares = self.shares

        order = orders.Order(
            self.uid, f"{self.uid}-{self.order_number}", side, shares, price,
        )
        self.order_number += 1
        return [order]


class MarketMakingAgent(Agent):
    """
    Provides liquidity on both sides of a limit order book in an attempt to collect
    small profits that result from other market participants crossing the spread.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"mma{cls.count}"

    def __init__(
        self,
        uid=None,
        shares=100,
        target_inventory_size=0,
        inventory_tolerance=25,
        window=10,
    ):
        super().__init__(uid)
        self.shares = shares
        self.target_inventory_size = target_inventory_size
        self.inventory_tolerance = inventory_tolerance
        self.shift = 0
        self.spread = 0.05
        self.hit_count = len(self.counter_parties)
        self.evolvable_parameters = [
            "shares",
            "target_inventory_size",
            "inventory_tolerance",
        ]
        self.param_bounds = [
            aa.IntervalBound(1, np.inf),
            aa.NoBound(),
            aa.PositiveRealBound(),
        ]

        self.window = window
        self.prices = np.zeros(self.window)
        self.current_ind = 0

    def __repr__(self):
        return "MarketMaking"

    def trade(self, eid):

        if self.engines[eid].eq == 'NaP':
            return []

        self.prices[self.current_ind] = self.engines[eid].eq
        self.current_ind = (self.current_ind + 1) % self.window
        p = self.prices[self.prices > 0].mean()

        # React to inventory imbalance
        divergence = self.shares_held - self.target_inventory_size
        if divergence > self.inventory_tolerance:
            self.shift = max(0.01, self.shift - 0.01)
            self.spread += 0.01
        elif divergence < -self.inventory_tolerance:
            self.shift += 0.01
            self.spread += 0.01
        else:
            self.shift = 0.0
            self.spread = max(0.01, self.spread - 0.01)

        # Place orders
        buy_order = orders.Order(
            self.uid,
            f"{self.uid}-{self.order_number}",
            "bid",
            max(int(self.shares - divergence), 10),
            np.round(p - self.spread + self.shift, 2),
            time_in_force=0,
        )
        self.order_number += 1

        sell_order = orders.Order(
            self.uid,
            f"{self.uid}-{self.order_number}",
            "ask",
            max(int(self.shares + divergence), 10),
            np.round(p + self.spread + self.shift, 2),
            time_in_force=0,
        )
        self.order_number += 1
        return [buy_order, sell_order]
