import functools

import numpy as np


def rsetattr(obj, attr, val):
    """
    https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-subobjects-chained-properties
    """
    pre, _, post = attr.rpartition(".")
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)


def rgetattr(obj, attr, *args):
    """
    https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-subobjects-chained-properties
    """

    def _getattr(obj, attr):
        return getattr(obj, attr, *args)

    return functools.reduce(_getattr, [obj] + attr.split("."))


def is_iterable(obj):
    try:
        _ = iter(obj)
    except TypeError:
        return False
    else:
        return True


def restamp(timestamp_array):
    """
    Restamps an array with duplicate timestamps to linearly inerpolate duplicates.

    Args:
        timestap_array: List[Int]

    Returns:
        newL List[Float]
    """
    timestamp_array = np.array(timestamp_array)
    new = list()
    new.append(timestamp_array[0])
    
    val = new[0]
    val_ix = 0
    same = 1
    for i, current in enumerate(timestamp_array[1:]):
        if current == val:
            same += 1
            continue
        else:
            frac = (current - val) / same
            for j in range(1, i - val_ix + 1):
                new.append(val + j * frac)
            new.append(current)
            same = 1
            val = current
            val_ix = i + 1

    if same > 1:
        new.extend([current] * (same - 1))

    return new
