import abc
import operator

import numpy as np

from . import agents, util


class Mutation(abc.ABC):
    def __init__(self, *args, base_agent=None, **kwargs):
        super().__init__()
        self.base_agent = base_agent or agents.Agent()

    def set_base_agent(self, agent):
        self.base_agent = agent

    @abc.abstractmethod
    def rvs(self, *args, **kwargs):
        pass


class SpeciesGaussianMutation(Mutation):
    def rvs(self, *args, size=1, gamma=0.5, **kwargs):
        new_agents = []

        for n in range(size):
            new_agent = type(self.base_agent)()

            for param_name, param_bound in zip(
                self.base_agent.evolvable_parameters, self.base_agent.param_bounds,
            ):
                old_param = operator.attrgetter(param_name)(self.base_agent)

                if util.is_iterable(old_param):
                    # should be iterable of objects with .shape attr defined
                    sub_params = []

                    for item in old_param:
                        noise = gamma * np.random.randn(*item.shape)
                        sub_param = item + noise
                        # check bounds
                        bounds = param_bound()
                        sub_param = np.clip(sub_param, bounds[0], bounds[1])
                        sub_params.append(sub_param)

                    util.rsetattr(new_agent, param_name, np.array(sub_params))

                else:
                    type_ = type(old_param)
                    noise = gamma * np.random.randn()
                    sub_param = old_param + noise
                    bounds = param_bound()
                    sub_param = type_(np.clip(sub_param, bounds[0], bounds[1]))
                    util.rsetattr(new_agent, param_name, sub_param)

            new_agents.append(new_agent)

        return new_agents
