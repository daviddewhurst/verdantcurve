import abc

import numpy as np


def relu(x):
    return np.where(x > 0, x, 0)


def softplus(x):
    return np.log(np.exp(x) + 1.0)


def softmax(x):
    z = np.exp(x)
    z = np.array([zi / np.sum(zi) for zi in z])
    return z


def db_trade_activation(x, max_abs_dp=1, max_abs_dshares=50):
    """Custom activation function for trading.

    Input must be 3-dimensional.
    """
    return [
        np.tanh(x[0]),
        np.where(
            np.absolute(x[1]) <= max_abs_dshares, x[1], np.sign(x[1]) * max_abs_dshares
        ),
        np.where(np.absolute(x[2]) <= max_abs_dp, x[2], np.sign(x[2]) * max_abs_dp),
    ]


def db_trade_activation_2(x):
    return [
        np.tanh(x[0]),
        relu(x[1]) + 1,
        relu(x[2]) + 0.01,
    ]


class Model(abc.ABC):
    def __init__(self):
        self.built = False
        self.layers = []
        self.biases = []

    @abc.abstractmethod
    def forward(self, x):
        pass


class Bound:
    def __init__(self):
        pass


class NoBound(Bound):
    def __init__(self):
        super().__init__()

    def __call__(self):
        return -np.inf, np.inf


class PositiveRealBound(Bound):
    def __init__(self):
        super().__init__()

    def __call__(self):
        return 0.0, np.inf


class IntervalBound(Bound):
    def __init__(self, a, b):
        super().__init__()
        self.a = a
        self.b = b

    def __call__(self):
        return self.a, self.b


class FFNN(Model):
    """Feed-forward neural network model.

    """

    def __init__(
        self,
        layer_dims=(2, 10, 5, 1),
        hidden_activation=relu,
        output_activation=softmax,
    ):
        super().__init__()
        self.layer_dims = layer_dims
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation

        for size_1, size_2 in zip(layer_dims[:-1], layer_dims[1:]):
            self.layers.append(0.05 * np.random.randn(size_1, size_2))
            self.biases.append(0.05 * np.random.randn() * np.ones(size_2))

        if self.layer_dims[-1] == 1:
            self.layers[-1] = self.layers[-1].flatten()

    def __repr__(self):
        return f"FFNN(layer_dims={self.layer_dims}, hidden_activation={self.hidden_activation}, output_activation={self.output_activation})"

    def get_shape(self):
        return self.layers, self.biases

    def forward(self, data):
        output = data

        for layer, bias in zip(self.layers[:-1], self.biases[:-1]):
            output = self.hidden_activation(np.dot(output, layer) + bias)
        return self.output_activation(np.dot(output, self.layers[-1]) + self.biases[-1])
