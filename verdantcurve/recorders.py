import operator

import numpy as np


def census(market):
    """
    Counts the number of individuals of each species in the market.

    Args:
        market: markets.Market

    Returns: Dict[str, int]
    """
    type2count = dict()
    for agent in market.registered_agents:
        class_name = type(agent).__name__
        try:
            type2count[class_name] += 1
        except KeyError:
            type2count[class_name] = 1
    return type2count


def track_evolvable_params(market, *species, reduce_funcs=(np.mean, np.std)):
    """
    """
    species_names = [s().__repr__() for s in species]
    param_holder = {
        name: {param_name: [] for param_name in s().evolvable_parameters}
        for name, s in zip(species_names, species)
    }
    for agent in market.registered_agents:
        name = agent.__repr__()
        if name in species_names:
            for param_name in agent.evolvable_parameters:
                param_value = operator.attrgetter(param_name)(agent)
                param_holder[name][param_name].append(param_value)
    # now create the reduced version
    collection = []
    for rf in reduce_funcs:
        collection.append(
            {
                k1: {k2: rf(v2) for k2, v2 in v1.items()}
                for k1, v1 in param_holder.items()
            }
        )
    return collection


def average_profit_by_species(market):
    """
    Computes average profit of each species in the market.

    Args:
        market: markets.Market

    Returns: Dict[str, float]
    """
    type2profit = dict()
    type2count = census(market)

    for agent in market.registered_agents:
        class_name = type(agent).__name__
        try:
            type2profit[class_name] += agent.profit
        except KeyError:
            type2profit[class_name] = agent.profit

    return {k: v / type2count[k] for k, v in type2profit.items()}


def rank_ordered_fitness_by_species(market):
    """
    Computes the rank fitness of each species in the market. Fitness is measured by average profit.

    Args:
        market: markets.Market

    Returns: Tuple[Tuple[Str,...], numpy.ndarray[Float]]
    """
    type2average_profit = average_profit_by_species(market)
    ranks = sorted(type2average_profit.items(), reverse=True, key=lambda t: t[-1])
    try:
        ranks, _ = zip(*ranks)
        return ranks, np.linspace(1, len(ranks), len(ranks))
    except ValueError:
        return [], []
