import json
import logging
import sys
from abc import ABC, abstractmethod
from collections import defaultdict

import numpy as np

from . import orders


def json2result(obj):
    """
    Converts a json object into a Result
    """
    return Result(
        int(obj["time"]),
        str(obj["engine_id"]),
        int(obj["shares"]),
        float(obj["price"]),
        str(obj["oid"]),
        str(obj["side"]),
        str(obj["poid"]),
    )


def is_order(order):
    return issubclass(type(order), orders.Order)


class Result:
    """
    Holds information associated with a trade executed by a matching engine.
    """

    def __init__(self, engine_id, oid, poid, shares, price, side, time):
        self.time = time
        self.engine_id = engine_id
        self.shares = shares
        self.price = price
        self.oid = oid
        self.side = side
        self.poid = poid

    def keys(self):
        return tuple([
            'time',
            'engine_id',
            'shares',
            'price',
            'oid',
            'side',
            'poid',
        ])

    def __getitem__(self, attr):
        try:
            return getattr(self, attr)
        except Exception as e:
            raise KeyError(f'{type(self)} has no attribute {attr}')

    def __str__(self):
        """
        Creates a log message, formatted as a JSON object, containing the trade information.

        Returns: str
        """
        return (
            f"{{time: {self.time}, mtype: trade, engine: {self.engine_id}, "
            f"shares:{self.shares}, price: {self.price}, oid: {self.oid}, "
            f"side: {self.side}, poid:{self.poid}}}"
        )

    def __repr__(self):
        return self.__str__()

    def to_json(
        self, filename=None, resource_dir="./",
    ):
        """
        Converts a Results into a json and (optionally) writes to file
        """
        obj = {
            "time": self.time,
            "engine_id": self.engine_id,
            "shares": self.shares,
            "price": self.price,
            "oid": self.oid,
            "side": self.side,
            "poid": self.poid,
        }
        if filename is not None:
            with open(resource_dir + filename, "w") as f:
                json.dump(obj, f)
            return
        return obj


class MatchingEngine(ABC):

    def __init__(self,):
        self.price_history = []
        self.volume_history = []

    @abstractmethod
    def submit(self, orders, book, t):
        pass

    @abstractmethod
    def match(self, book, t):
        pass


class BatchEngine(MatchingEngine):
    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"b{cls.count}"

    def __init__(self, eid=None, eq=100, luld_pct=0.05, luld_window=10):
        super().__init__()
        self.eid = eid or type(self).next_id()
        self.agent_updates = defaultdict(list)

        self.eq = eq
        self.luld_pct = luld_pct
        self.luld_window = luld_window
        self.price_history = []
        self.traded_at = []
        self.volume = 0
        self.batch_number = 0

        self.luld = (None, None)
        self.update_luld_bands()

        self.book = None

    def update_luld_bands(self):
        """
        Calculates Limit Up - Limit Down price boundaries that can be used to restrict trading
        during periods of instability.

        Returns: Tuple[float, float]
            Lower limit and upper limit respectively.
        """
        if self.price_history:
            w_price = np.nanmean(self.price_history[-self.luld_window :])
            self.luld = np.round(
                [w_price * (1.0 - self.luld_pct), w_price * (1.0 + self.luld_pct)],
                decimals=2,
            )
        else:
            if not np.any(np.isnan(self.eq)):
                self.luld = np.round(
                    [self.eq * (1.0 - self.luld_pct), self.eq * (1.0 + self.luld_pct)],
                    decimals=2,
                )
            else:
                logging.warning(
                    f"Currently eq is NaN, turning LULD bands off for this match"
                )
                self.luld = (0.01, sys.float_info.max)

        logging.info(f"Current LULD bands: {self.luld}")

    def submit(self, orders, book, t):
        for order in orders:
            order.submit_time = self.batch_number

            if order.o_type == "market":
                order.price = self.luld[0] if order.side == "ask" else self.luld[1]
                if np.any(np.isnan(order.price)):
                    logging.info(
                        f"One of luld bands is currently NaN, market orders will not match"
                    )
                    continue

            book.add_order(order)

    def match(self, book, t):
        """
        Matches a batch of resting orders in a discrete time double-sided auction.

        Matching occurs in price-time priority order, where asks with a lower
        price/bids with a higher price are matched first, ties in price are
        broken by age, with older orders matching first. Ties in age are broken
        randomly. Note that orders submitted during the same batch will have an
        identical age.

        Attempts to provide more favorable prices to singular orders that cross the
        spread during periods of low liquidity.

        Args:
            book: OrderBook, Resting orders.
            t: numeric, Current time.
        """
        self.book = book
        self.volume = 0

        if not book.asks or not book.bids:
            return

        best_bid = book.bids[0]
        bb_price = best_bid.price
        best_ask = book.asks[0]
        ba_price = best_ask.price
        if bb_price < ba_price:
            return

        # If there is exactly one crossing buyer (and multiple crossing sellers)
        # or exactly one crossing seller and many crossing buyers, then matching
        # should follow CDA-like behavior assuming that the orders from the single
        # participant are active. This will provide crossing orders with better
        # execution prices during periods with low volume. Note that the FBA
        # mechanism provides all participants with better execution prices during
        # periods with high volume.
        n_crossing_bmps = len({x.uid for x in book.bids if x.price >= ba_price})
        n_crossing_amps = len({x.uid for x in book.asks if x.price <= bb_price})

        if (
            (n_crossing_bmps == 1)
            and (n_crossing_amps > 1)
            and (best_bid.submit_time == t)
        ):
            logging.info("Non-uniform matching triggered: buy side advantaged")
            price = self.execute_nonuniform_match(book, t, best_bid)
        elif (
            (n_crossing_amps == 1)
            and (n_crossing_bmps > 1)
            and (best_ask.submit_time == t)
        ):
            logging.info("Non-uniform matching triggered: sell side advantaged")
            price = self.execute_nonuniform_match(book, t, best_ask)
        else:
            price = self.execute_uniform_match(book, t)

        # Match until no longer crossing
        if (len(book.asks) > 0) and (len(book.bids) > 0) and book.crossed_market():
            logging.warning(
                f"Book still crossed following batch, "
                f"BB @ {book.bids[0].price} BO @ {book.asks[0].price}"
            )
            self.match(book, t)
        else:
            self.traded_at.append(t)
            self.price_history.append(price)
            self.batch_number += 1
        self.volume_history.append(self.volume)

    def execute_uniform_match(self, book, t):
        """
        Performs the matching procedure of a discrete time double auction using
        a uniform execution price.

        Args:
            book: OrderBook, Resting orders.
            t: numeric, current time.
        """
        price = self.select_price(book)
        if price == orders.NaP:
            return

        a2match = [a for a in book.asks if a.price <= price]
        b2match = [b for b in book.bids if b.price >= price]

        an, bn = 0, 0
        while (an < len(a2match)) and (bn < len(b2match)):
            a, b = a2match[an], b2match[bn]

            if a.shares == b.shares:
                shares = a.shares
                book.remove_orders([a, b], t)
                an += 1
                bn += 1

            elif a.shares >= b.shares:
                shares = b.shares
                a.shares -= b.shares
                book.remove_order(b, t)
                bn += 1

            else:
                shares = a.shares
                b.shares -= a.shares
                book.remove_order(a, t)
                an += 1

            a_result = Result(self.eid, a.oid, b.oid, shares, price, "ask", t)
            b_result = Result(self.eid, b.oid, a.oid, shares, price, "bid", t)
            logging.info(a_result)

            self.agent_updates[a.uid].append(a_result)
            self.agent_updates[b.uid].append(b_result)
            self.volume += shares
        return price

    def execute_nonuniform_match(self, book, t, price_side):
        """
        Performs the matching procedure for a discrete time double auction using
        a non-uniform execution price.

        Args:
            book: OrderBook, Resting orders.
            t: numeric, Current time.
            price_side: str, Indicates the side of the market that will determine execution prices.
        """
        prices = []
        while book.crossed_market():
            a, b = book.asks[0], book.bids[0]

            if price_side == "bid":
                price = b.price
            else:
                price = a.price
            prices.append(price)

            if a.shares == b.shares:
                shares = a.shares
                book.remove_orders([a, b], t)

            elif a.shares >= b.shares:
                shares = b.shares
                a.shares -= b.shares
                book.remove_order(b, t)

            else:
                shares = a.shares
                b.shares -= a.shares
                book.remove_order(a, t)

            a_result = Result(self.eid, a.oid, b.oid, shares, price, "ask", t)
            b_result = Result(self.eid, b.oid, a.oid, shares, price, "bid", t)
            logging.info(a_result)

            self.agent_updates[a.uid].append(a_result)
            self.agent_updates[b.uid].append(b_result)
            self.volume += shares
        return np.mean(prices)

    def select_price(self, book):
        """
        Selects the uniform execution price that maximizes the expected number
        of shares executed in a batch.

        Augmented with additional logic following:
            http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.196.7163&rep=rep1&type=pdf

        Returns: float
            Selected execution price.
        """
        bids, asks = book.get_consolidated_book()

        best_bid = book.bids[0].price
        best_ask = book.asks[0].price

        if best_bid < best_ask:
            logging.info(f"No crossed orders: BB = {best_bid}, BO = {best_ask}.")
            return orders.NaP
        elif best_bid == best_ask:
            logging.info(f"Locked market: BB = BO = {best_bid}.")
            self.eq = best_bid
        else:
            price_count = int(np.round((best_bid - best_ask) / 0.01))
            crossed_prices = np.round(np.linspace(best_ask, best_bid, price_count,), 2)
            bid_volume = np.zeros_like(crossed_prices)
            ask_volume = np.zeros_like(crossed_prices)

            for i, price in enumerate(crossed_prices):
                ask_volume[i] += asks.get(price, 0.0)
                bid_volume[i] += bids.get(price, 0.0)

            cum_buy_quantity = np.cumsum(bid_volume[::-1])[::-1]
            cum_sell_quantity = np.cumsum(ask_volume)

            executable_vol = np.minimum(cum_buy_quantity, cum_sell_quantity)
            inds = np.nonzero(executable_vol == executable_vol.max())

            if len(inds) > 1:
                quantity_diff = cum_buy_quantity - cum_sell_quantity
                surplus = np.abs(quantity_diff)
                inds = np.nonzero(surplus == surplus.min())

            prices = crossed_prices[inds]

            if len(prices) > 1:
                logging.warning(f"Multiple viable clearing prices: {prices}")

            self.eq = np.median(prices)

        self.eq = np.round(self.eq * 200) / 200
        logging.info(f"New uniform execution price @ {self.eq}")
        return self.eq


class ContinuousEngine(MatchingEngine):
    """
    Implements a matching engine with Continuous Double Auction semantics.
    Orders are inserted into the book as soon as they are received and matching
    occurs as soon as possible.
    """

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f"c{cls.count}"

    def __init__(self, eid=None, eq=orders.NaP, luld_pct=0.05, luld_window=10):
        super().__init__()

        self.eid = eid or type(self).next_id()
        self.agent_updates = defaultdict(list)
        self.luld_pct = luld_pct
        self.luld_window = luld_window

        self.eq = eq
        self.traded_at = []
        self.volume = 0

    def update_luld_bands(self):
        """
        Calculates Limit Up - Limit Down price boundaries that can be used to restrict trading
        during periods of instability.

        Returns: Tuple[float, float]
            Lower limit and upper limit respectively.
        """
        if self.price_history:
            w_price = np.nanmean(self.price_history[-self.luld_window :])
            self.luld = np.round(
                [w_price * (1.0 - self.luld_pct), w_price * (1.0 + self.luld_pct)],
                decimals=2,
            )
        else:
            if self.eq == orders.NaP:
                logging.warning(
                    f"Currently eq is NaN, turning LULD bands off for this match"
                )
                self.luld = (0.01, sys.float_info.max)
            elif not np.any(np.isnan(self.eq)):
                self.luld = np.round(
                    [self.eq * (1.0 - self.luld_pct), self.eq * (1.0 + self.luld_pct)],
                    decimals=2,
                )
            else:
                logging.warning(
                    f"Currently eq is NaN, turning LULD bands off for this match"
                )
                self.luld = (0.01, sys.float_info.max)

        logging.info(f"Current LULD bands: {self.luld}")

    def submit(self, orders, book, t):
        """
        Receives orders from market participants that are either matched immediately
        or inserted into the book.

        Args:
            order: Order, Submitted order.
            book: OrderBook, Holds resting orders.
            t:  numeric, Current time.
        """
        for order in orders:
            if not is_order(order) or not order.price:
                continue

            order.submit_time = t
            prices = []

            # Match the order, if possible
            if order.side == "bid":
                a2match = [a for a in book.asks if a.price <= order.price]
                while order.shares and len(a2match):
                    best_ask = a2match[0]
                    if order.shares == best_ask.shares:
                        shares = order.shares
                        book.remove_order(best_ask, t)
                    elif order.shares >= best_ask.shares:
                        shares = best_ask.shares
                        book.remove_order(best_ask, t)
                        del a2match[0]
                    else:
                        shares = order.shares
                        best_ask.shares -= order.shares
                    order.shares -= shares

                    a_result = Result(
                        self.eid, best_ask.oid, order.oid, shares, best_ask.price, "ask", t
                    )
                    b_result = Result(
                        self.eid, order.oid, best_ask.oid, shares, best_ask.price, "bid", t
                    )
                    logging.info(a_result)

                    self.agent_updates[best_ask.uid].append(a_result)
                    self.agent_updates[order.uid].append(b_result)

                    self.volume += shares
                    prices.append(best_ask.price)

            else:
                b2match = [b for b in book.bids if b.price >= order.price]
                while order.shares and len(b2match):
                    best_bid = b2match[0]
                    if order.shares == best_bid.shares:
                        shares = order.shares
                        book.remove_order(best_bid, t)
                    elif order.shares >= best_bid.shares:
                        shares = best_bid.shares
                        book.remove_order(best_bid, t)
                        del b2match[0]
                    else:
                        shares = order.shares
                        best_bid.shares -= order.shares
                    order.shares -= shares

                    a_result = Result(
                        self.eid, order.oid, best_bid.oid, shares, best_bid.price, "ask", t
                    )
                    b_result = Result(
                        self.eid, best_bid.oid, order.oid, shares, best_bid.price, "bid", t
                    )
                    logging.info(a_result)

                    self.agent_updates[order.uid].append(a_result)
                    self.agent_updates[best_bid.uid].append(b_result)

                    self.volume += shares
                    prices.append(best_bid.price)

            # If there are shares remaining, place the order in the book
            if order.shares:
                book.add_order(order)

            # Log activity if matching has occurred
            if prices and self.volume:
                self.price_history.append(np.mean(prices))
                self.eq = self.price_history[-1]
                self.traded_at.append(t)

            self.volume_history.append(self.volume)

    def match(self, book, t):
        """
        Matching occurs in the submit function in order to implement the desired
        continuous trading semantics while maintaining the same interface.

        Args:
            book: OrderBook, Holds resting orders.
            t: numeric, Current time.
        """
        pass
