import logging
import os
import random
import time
from pathlib import Path

from . import orders
from .engine import BatchEngine

STOPMATCHING = "STOPMATCHING"


class OrderPipeline:
    def __init__(
        self, engine, book, resource="directory", resource_loc=Path(),
    ):
        self.engine = engine
        self.book = book

        self.resource = resource
        self.resource_loc = resource_loc

    def insert_into_book(self, t, loc=None):
        if self.resource == "directory":
            self._insert_from_directory_into_book(t, loc=loc)
        else:
            raise NotImplementedError("Only directory method currently implemented")

    def _insert_from_directory_into_book(self, t, loc=None):
        """
        Read formatted json objects stored in a directory and process into orderbook.

        Ensures that objects in directory already read are not processed into orderbook.
        """
        if not loc:
            loc = self.resource_loc

        orders_to_submit = []
        for file in loc.glob("*"):
            with open(file, "r") as f:
                try:
                    order = orders.read_json(f)
                    if order not in self.book:
                        orders_to_submit.append(order)
                except Exception as e:
                    logging.warning(f"Skipping {file} due to loading error: {e}")
                    continue
            file.unlink()

        random.shuffle(orders_to_submit)
        self.engine.submit(orders_to_submit, self.book, t)


class DistributedMarket:
    """
    """

    def __init__(
        self,
        uids=None,
        book=None,
        m_engine=None,
        pipeline=None,
        resource="directory",
        resource_loc="./",
        steps=20,
        max_sleep=2,
        status_resource="directory",
        status_resource_loc="./",
    ):
        self.registered_uids = uids or []
        self.book = book or orders.OrderBook()
        self.engine = m_engine or BatchEngine()
        self.resource = resource
        self.resource_loc = resource_loc
        self.pipeline = pipeline or OrderPipeline(
            self.engine,
            self.book,
            resource=self.resource,
            resource_loc=self.resource_loc,
        )
        self.pipeline.insert_into_book(-1)
        self.steps = steps

        self.maxsleep = max_sleep
        self.status_resource = status_resource
        self.status_resource_loc = status_resource_loc

    def register_uids(self, uids):
        """
        Registers new uids with the matching engine
        """
        logging.info("{} new agents entering market".format(len(uids)))
        self.registered_uids.extend(uids)

    def delete_uids(self, uids, time):
        """
        Removes one or more uids from the list of active uids. Ensures that
        all orders associated with removed uids are removed from the limit
        order book.

        Args:
            uids: List[str]
            time: numeric, Current simulation time.
        """
        for uid in uids:
            self.book.remove_agent(uid, time)

            try:
                self.registered_uids.remove(uid)
                logging.info("uid {} left the market".format(uid))
            except ValueError:
                logging.info(
                    "uid {} did not reach the market before removal".format(uid)
                )

    def step(self, t):
        """
        """
        self.pipeline.insert_into_book(t)
        self.engine.match(self.book, t)
        self.book.remove_stale(t)
        self.engine.update_luld_bands()

        order_count = len(self.book.bids) + len(self.book.asks)
        logging.info(
            f"{order_count} orders, "
            f"{100 * len(self.book.bids) / (order_count + 1e-5):0.2f}% bids, "
            f"{100 * len(self.book.asks) / (order_count + 1e-5):0.2f}% asks"
        )

    def get_status_message(self):
        """
        Gets matching engine status message.
        Used to allow the market to operate for unknown periods of time.
        Termination may be triggered by issuing the STOPMATCHING status message.
        """
        if self.status_resource == "directory":
            status_files = os.listdir(self.status_resource_loc)
            if STOPMATCHING in status_files:
                return False
            return True
        else:
            raise NotImplementedError(
                "Currently only directory status resource method implemented"
            )

    def run(self, *args, **kwargs):
        """
        """
        t = 0
        while True:
            try:
                status = self.get_status_message()
                logging.info(f'Current status: {"match" if status else "halt"}')
                if status:
                    logging.info("Waiting random time for next batch")
                    time.sleep(self.maxsleep * random.random())
                    t += 1
                    logging.info(f"Matching batch {t}")
                    logging.info(
                        f"Total number of orders in db before match: {len(os.listdir(self.resource_loc))}"
                    )
                    self.step(t)
                    logging.info(
                        f"Total number of orders in db after match: {len(os.listdir(self.resource_loc))}"
                    )
                    logging.info(f"New price: {self.engine.eq}")

                    if t % self.steps == 0:
                        self.book.clear_book(t)
                else:
                    break

            except KeyboardInterrupt:
                break
        return
