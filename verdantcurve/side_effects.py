import abc

import numpy as np

from . import agents, evo, recorders


class SelectionMechanism(abc.ABC):
    """
    ABC for selection mechanism side effects. Selection mechanisms must implement
    .select(market, *args, **kwargs) methods.
    """

    def __init__(self, *args, **kwargs):
        super().__init__()

    @abc.abstractmethod
    def select(self, market, *args, **kwargs):
        pass


class ReplacementMechanism(abc.ABC):
    """
    ABC for replacement mechanism side effects. Replacement mechanisms must
    implement .replace(market, *args, **kwargs) methods.
    """

    def __init__(self, *args, **kwargs):
        super().__init__()

    @abc.abstractmethod
    def replace(self, market, *args, **kwargs):
        pass


class TournamentSelection(SelectionMechanism):
    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)

    def select(self, market, *args, **kwargs):
        """
        Performs rank-fitness tournament selection.  
        """
        size = kwargs.get("size", 10)
        p = kwargs.get("p", 0.33)
        return_values = kwargs.get("return_values", False)
        try:
            candidates = np.random.choice(
                market.registered_agents, size=size, replace=False
            )
            sorted_candidates = sorted(
                candidates, reverse=True, key=lambda x: x.profit,
            )
            # Probabilistic truncation of agents
            for i, agent in enumerate(sorted_candidates):
                if np.random.random() < p:
                    continue
                else:
                    break
            # Ditch the rest of them
            market.delete_agents(sorted_candidates[i:], market.current_time)
            if return_values:
                return agent, len(sorted_candidates[i:])
        except ValueError:
            return


class RankFitnessProportionateReplacement(ReplacementMechanism):
    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.exclude = kwargs.get("exclude", [])

    def replace(self, market, *args, **kwargs):
        species2profit = kwargs.get("species2profit", tuple())
        species, ranks = species2profit
        probs = 1.0 / ranks / np.sum(1.0 / ranks)

        old_species2count = kwargs.get("species2count", {"null": 100})
        new_species2count = recorders.census(market)
        old_n = np.sum(list(old_species2count.values()))
        new_n = np.sum(list(new_species2count.values()))
        diff = old_n - new_n

        inds = np.random.choice(probs.shape[0], p=probs, size=diff)
        new_agents = [getattr(agents, species[ind])() for ind in inds]
        market.register_agents(new_agents)


class SpeciesGaussianMutationReplacement(ReplacementMechanism):
    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.mutation = evo.SpeciesGaussianMutation()

    def replace(self, market, *args, **kwargs):
        n_to_add = kwargs.get("n_to_add", 1)
        base_agent = kwargs.get("base_agent", agents.Agent())
        self.mutation.set_base_agent(base_agent)
        new_agents = self.mutation.rvs(size=n_to_add)
        market.register_agents(new_agents)


class TournamentSelectionSpeciesGaussianMutation(
    SelectionMechanism, ReplacementMechanism
):
    """
    A selection and replacement mechanism. Agents are selected using a rank-fitness
    tournament selection. The population is conserved; a
    number of new agents equal to the number selected out is added.
    New agents are mutated versions of the agent selected to reproduced in the tournament selection.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.event_prob = kwargs.get("event_prob", 0.1)
        self.selection_mechanism = TournamentSelection()
        self.replacement_mechanism = SpeciesGaussianMutationReplacement()

    def evolutionary_step(self, market):
        """
        The evolutionary step of the mechanism, selection -> replacement

        Args:
            market: markets.Market
        """
        if np.random.random() < self.event_prob:
            base_agent, n_to_add = self.select(market, return_values=True)
            self.replace(market, base_agent=base_agent, n_to_add=n_to_add)

    def select(self, market, *args, **kwargs):
        return self.selection_mechanism.select(market, *args, **kwargs)

    def replace(self, market, *args, **kwargs):
        self.replacement_mechanism.replace(market, *args, **kwargs)


class TournamentSelectionFPReplacement(SelectionMechanism, ReplacementMechanism):
    """
    A selection and replacement mechanism. Agents are selected using a rank-fitness 
    tournament selection. The population is conserved; a
    number of new agents equal to the number selected out is added.
    New agents are added with their species drawn with probability inversely proportionate
    to the average fitness of that species. 
    """

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.exclude = kwargs.get("exclude", [])
        self.selection_mechanism = TournamentSelection()
        self.replacement_mechanism = RankFitnessProportionateReplacement(
            exclude=self.exclude
        )

    def evolutionary_step(self, market):
        """
        The evolutionary step of the mechanism, selection -> replacement
        
        Args:
            market: markets.Market
        """
        species2count = recorders.census(market)
        species2profit = recorders.rank_ordered_fitness_by_species(market)
        self.select(market)
        self.replace(
            market, species2count=species2count, species2profit=species2profit,
        )

    def select(self, market, *args, **kwargs):
        self.selection_mechanism.select(market)

    def replace(self, market, *args, **kwargs):
        self.replacement_mechanism.replace(market, *args, **kwargs)
