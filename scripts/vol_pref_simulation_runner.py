#!/usr/bin/env python

import subprocess
import sys

import numpy as np


def main(n_reruns=50):
    vols = np.logspace(np.log10(0.5), np.log10(20), 10)

    for r in range(n_reruns):
        for vol in vols:
            subprocess.call(f'qsub -v SEED={r},VOL={vol} vpsr.pbs', shell=True)

    sys.exit(0)


if __name__ == "__main__":
    main()
