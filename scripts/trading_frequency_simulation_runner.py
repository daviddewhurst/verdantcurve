#!/usr/bin/env python

import subprocess
import sys

import numpy as np


def main(n_reruns=100,):
    for r in range(n_reruns):
        subprocess.call(f'qsub -v SEED={r} tfsr_unif.pbs', shell=True)

        for pp in np.linspace(0.1, 0.9, 5):
            subprocess.call(f'qsub -v SEED={r},PP={pp} tfsr_fixed.pbs', shell=True)

        subprocess.call(f'qsub -v SEED={r} tfsr_beta.pbs', shell=True)

    sys.exit(0)


if __name__ == "__main__":
    main()
