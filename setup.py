from pathlib import Path

import setuptools

# Package meta-data.
NAME = "verdantcurve"
DESCRIPTION = "A market microstructure simulation framework."
URL = "https://gitlab.com/daviddewhurst/verdantcurve"
EMAIL = "drd@davidrushingdewhurst.com"
AUTHOR = "David Dewhurst, Colin Van Oort"
REQUIRES_PYTHON = ">=3.6.0"
VERSION = 0.1

# Required packages
with open("requirements.txt", "r") as f:
    REQUIRED = [x.strip() for x in f]

# Optional packages
EXTRAS = {}

# Path to package top-level
here = Path(__file__).parent.resolve()

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
try:
    with open(here / "README.md", "r") as f:
        long_description = "\n" + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    with open(here / NAME / "__version__.py") as f:
        exec(f.read(), about)
else:
    about["__version__"] = VERSION

setuptools.setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type="text/markdown",
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=setuptools.find_packages(exclude=("tests",)),
    scripts=[
        "scripts/analyze_orderbook",
        "scripts/log2animation",
        "scripts/simulation",
    ],
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license=None,
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
)
