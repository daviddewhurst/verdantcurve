from pathlib import Path

import pytest

from verdantcurve.distributed_markets import STOPMATCHING


def delete_folder(p):
    """
    Borrowed from:
        https://stackoverflow.com/questions/303200/how-do-i-remove-delete-a-folder-that-is-not-empty
    """
    for sub in p.iterdir():
        if sub.is_dir():
            delete_folder(sub)
        else:
            sub.unlink()
    p.rmdir()


@pytest.fixture
def resource_dir():
    p = Path(".tmp")
    p.mkdir(exist_ok=True, parents=True)

    yield p

    delete_folder(p)


@pytest.fixture
def stop_matching():
    """
    Used to prevent tests of DistributedMarket.run from not terminating.
    """
    p = Path(STOPMATCHING)
    p.touch()

    yield p

    p.unlink()
