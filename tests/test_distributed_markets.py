import multiprocessing
import time

import numpy as np

from verdantcurve import distributed_markets as dm
from verdantcurve import orders


def start_exchange(resource_dir):
    dm.DistributedMarket(resource_loc=resource_dir, max_sleep=0.0, steps=100,).run()


def test_order_serialization(resource_dir, n_iters=100, n_orders=100, avg_delay=0):
    """
    Random order generation and serialization.
    """
    onum = 0

    for n in range(n_iters):
        for i in range(n_orders):
            uid = np.random.choice(["dave", "casey"])
            side = np.random.choice(["bid", "ask"])
            oid = uid + str(onum)

            order = orders.Order(
                uid,
                oid,
                side,
                np.random.poisson(1000),
                100 + 5 * round(np.random.randn(), 2),
            )

            order.to_json(path=resource_dir / f"{oid}.json")
            onum += 1

        # sleep to simulate processing time
        time.sleep(np.random.exponential(avg_delay))


def test_distributed_market_init(resource_dir, uid="dave", n_orders=500):
    for i in range(n_orders):
        oid = uid + str(i)
        order = orders.Order(
            uid,
            oid,
            np.random.choice(["bid", "ask"]),
            np.random.poisson(100),
            100 + 5 * round(np.random.randn(), 2),
        )
        order.to_json(path=resource_dir / f"{oid}.json")
    assert len(list(resource_dir.glob("*"))) == n_orders

    mkt = dm.DistributedMarket(resource_loc=resource_dir)
    assert len(mkt.book.bids) + len(mkt.book.asks) == n_orders
    assert len(list(resource_dir.glob("*"))) == 0
    mkt.step(0)
    assert len(mkt.book.bids) + len(mkt.book.asks) <= n_orders


def test_distributed_market_run(resource_dir, stop_matching):
    start_exchange(resource_dir)


def test_distributed_market_integration(resource_dir):
    market = multiprocessing.Process(target=start_exchange, args=(resource_dir,),)
    agents = multiprocessing.Process(
        target=test_order_serialization,
        args=(resource_dir,),
        kwargs={"avg_delay": 0, "n_iters": 100, "n_orders": 100,},
    )

    market.start()
    agents.start()
    agents.join()
    time.sleep(0.1)
    market.terminate()
    agents.terminate()
