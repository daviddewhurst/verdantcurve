import numpy as np

from verdantcurve import agent_architectures as aa


def test_ffnn():
    nn = aa.FFNN(layer_dims=(3, 25, 5), output_activation=aa.db_trade_activation,)

    X = np.random.randn(100, 3)
    test = nn.forward(X)

    assert len(test) == 3
    for e in test:
        assert e.shape == (5,)
