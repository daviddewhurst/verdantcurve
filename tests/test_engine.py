import numpy as np

from verdantcurve import engine, orders


def make_fake_order(
        uid_range=10000,
        oid_range=10000,
        share_range=10000,
        price_range=10000,
        time_in_force_range=10000,
):
    return orders.Order(
        uid=np.random.randint(uid_range),
        oid=np.random.randint(oid_range),
        side=np.random.choice(['bid', 'ask']),
        shares=np.random.randint(share_range),
        price=np.random.random() * price_range,
        o_type=np.random.choice(['limit', 'market']),
        time_in_force=np.random.randint(time_in_force_range),
    )


class TestBatchEngine:
    def test_submit(self):
        e = engine.BatchEngine()
        book = orders.OrderBook()

        order_count = 0
        for i in range(1000):
            orders_ = [make_fake_order() for _ in range(np.random.randint(100))]
            order_count += len(orders_)
            e.submit(
                orders_,
                book,
                i,
            )

        assert order_count == (len(book.bids) + len(book.asks))


class TestContinuousEngine:
    pass
