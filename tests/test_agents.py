"""
These tests are fairly flimsy at the moment.
Will try to improve the situation ASAP.
"""
import numpy as np

from verdantcurve import agents, orders


class MockEngine:
    def __init__(self):
        self._eq = np.random.random() * 100

    @property
    def eq(self):
        self._eq += 2 * np.random.random() - 1
        return self._eq


class TestZIAgent:
    def test_id_increment(self):
        self.agent = agents.ZIAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.ZIAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.ZIAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestZIPAgent:
    def test_id_increment(self):
        self.agent = agents.ZIPAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.ZIPAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.ZIPAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestMomentumAgent:
    def test_id_increment(self):
        self.agent = agents.MomentumAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.MomentumAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.MomentumAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestFundamentalValueAgent:
    def test_id_increment(self):
        self.agent = agents.FundamentalValueAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.FundamentalValueAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.FundamentalValueAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestMeanReversionAgent:
    def test_id_increment(self):
        self.agent = agents.MeanReversionAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.MeanReversionAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.MeanReversionAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestMarketMakingAgent:
    def test_id_increment(self):
        self.agent = agents.MarketMakingAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.MarketMakingAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.MarketMakingAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestFFNNAgent:
    def test_id_increment(self):
        self.agent = agents.FFNNAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.FFNNAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.FFNNAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)


class TestFFNNPAgent:
    def test_id_increment(self):
        self.agent = agents.FFNNPAgent()
        self.agent.engines[0] = MockEngine()

        old_count = type(self.agent).count
        agents.FFNNPAgent()
        new_count = type(self.agent).count
        assert old_count + 1 == new_count

    def test_trade(self):
        self.agent = agents.FFNNPAgent()
        self.agent.engines[0] = MockEngine()

        trades = self.agent.trade(0)
        assert not trades or isinstance(trades[0], orders.Order)
